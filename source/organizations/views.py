from django_filters import rest_framework as filters
from rest_framework import generics
from rest_framework.generics import get_object_or_404
from rest_framework.permissions import AllowAny
from rest_framework.response import Response

from organizations.filters import OrganizationListFilter
from organizations.models import Organization, District
from organizations.serializers.organizations import OrganizationDetailSerializer, OrganizationListSerializer


class OrganizationDetail(generics.RetrieveAPIView):
    queryset = Organization.objects.all()
    serializer_class = OrganizationDetailSerializer
    permission_classes = (AllowAny,)


class OrganizationList(generics.ListAPIView):
    queryset = Organization.objects.all()
    serializer_class = OrganizationListSerializer
    permission_classes = (AllowAny,)
    filter_class = OrganizationListFilter


class OrganizationByDistrictList(generics.ListAPIView):
    queryset = Organization.objects.all()
    serializer_class = OrganizationListSerializer
    permission_classes = (AllowAny,)
    filter_backends = (filters.DjangoFilterBackend,)
    filter_class = OrganizationListFilter

    def list(self, request, *args, **kwargs):
        district_id = kwargs['district_id']
        district = get_object_or_404(District, pk=district_id)
        organizations = district.organizations.all().prefetch_related('district').prefetch_related('product_prices')
        organizations = self.filter_queryset(organizations).distinct()
        serializer = self.get_serializer(organizations, many=True)
        return Response(serializer.data)
