from rest_framework import serializers
from organizations.models import Organization, District
from products.serializers.product import ProductPriceSerializer


class DistrictSerializer(serializers.ModelSerializer):
    class Meta:
        model = District
        fields = '__all__'


class OrganizationDetailSerializer(serializers.ModelSerializer):
    district = DistrictSerializer(many=True)
    organization_network = serializers.StringRelatedField()
    product_prices = ProductPriceSerializer(many=True)

    class Meta:
        model = Organization
        fields = '__all__'


class OrganizationListSerializer(serializers.ModelSerializer):

    class Meta:
        model = Organization
        fields = ('id', 'title', 'district')
